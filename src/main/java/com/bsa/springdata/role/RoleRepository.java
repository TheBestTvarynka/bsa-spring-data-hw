package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
    @Transactional
    @Query(value = "delete from roles " +
            "using (select roles.id as role_id, user2role.user_id as user_id from roles left join user2role on user2role.role_id = roles.id) as ur " +
            "where ur.role_id = roles.id " +
            "and ur.user_id is null " +
            "and roles.code = :code",
    nativeQuery = true)
    void deleteUnusedRolesByCode(String code);

}
