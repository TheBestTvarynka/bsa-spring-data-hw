package com.bsa.springdata.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Use this interface when you make a projection from native query.
//  If you don't use native query replace this interface with a simple POJO
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectSummaryDto {
    String name;
    int teamsNumber;
    int developersNumber;
    String technologies;
}