package com.bsa.springdata.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(value = "select cast(projects.id as varchar), projects.name, projects.description " +
            "from projects " +
            "left join teams on teams.project_id = projects.id " +
            "left join technologies on technologies.id = teams.technology_id " +
            "left join (select teams.project_id as project_id, count(users.id) as dc from users left join teams on users.team_id = teams.id group by teams.project_id) as dcount on dcount.project_id = projects.id " +
            "where technologies.name = :technology " +
            "order by dc " +
            "limit 5",
            nativeQuery = true)
    List<Object[]> findTop5ByTechnology(String technology);

    @Query(value = "select projects.name, tcount.tc, dcount.dc, tcs.tnames " +
            "from projects " +
            "left join (select project_id, count(id) as tc from teams group by project_id) as tcount on tcount.project_id = projects.id " +
            "left join (select teams.project_id as project_id, count(users.id) as dc from users left join teams on users.team_id = teams.id group by teams.project_id) as dcount on dcount.project_id = projects.id " +
            "left join (select teams.project_id as project_id, string_agg(technologies.name, ',') as tnames from teams left join technologies on technologies.id = teams.technology_id group by teams.project_id) as tcs on tcs.project_id = projects.id " +
            "order by projects.name",
            nativeQuery = true)
    List<Object[]> projectsSummary();

    @Query(value = "select cast(projects.id as varchar), projects.name, projects.description " +
            "from projects " +
            "left join (select project_id, count(id) as tc from teams group by project_id) as tcount on tcount.project_id = projects.id " +
            "left join (select teams.project_id as project_id, count(users.id) as dc from users left join teams on users.team_id = teams.id group by teams.project_id) as dcount on dcount.project_id = projects.id " +
            "order by tcount.tc desc, dcount.dc desc, projects.name " +
            "desc limit 1",
            nativeQuery = true)
    List<Object[]> findTheBiggest();

//    @Query("select count(p) from Project p ")
//    int getCountProjectsWithRole(String role);

}