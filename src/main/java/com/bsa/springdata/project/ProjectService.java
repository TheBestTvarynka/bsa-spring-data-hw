package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    private ProjectDto rowArrayToProjectDto(Object[] rawArray) {
        return new ProjectDto(UUID.fromString((String) rawArray[0]), (String) rawArray[1], (String) rawArray[2]);
    }

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        // Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        return projectRepository.findTop5ByTechnology(technology).stream()
                .map(this::rowArrayToProjectDto)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        // Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        List<ProjectDto> projects = projectRepository.findTheBiggest().stream()
                .map(this::rowArrayToProjectDto)
                .collect(Collectors.toList());
        if (projects.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(projects.get(0));
    }

    public List<ProjectSummaryDto> getSummary() {
        // Try to use native query and projection first. If it fails try to make as few queries as possible
        return projectRepository.projectsSummary().stream()
                .map(elem -> new ProjectSummaryDto(
                        (String) elem[0],
                        ((BigInteger) elem[1]).intValue(),
                        ((BigInteger) elem[2]).intValue(),
                        (String) elem[3]))
                .collect(Collectors.toList());
    }

    public int getCountWithRole(String role) {
        // TODO: Use a single query
        return -1;
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        // Use common JPARepository methods. Build entities in memory and then persist them
        Team newTeam = new Team();

        Project newProject = new Project();
        newProject.setName(createProjectRequest.getProjectName());
        newProject.setDescription(createProjectRequest.getProjectDescription());
        newProject.setTeams(List.of(newTeam));
        Project project = projectRepository.save(newProject);

        Technology newTechnology = new Technology();
        newTechnology.setName(createProjectRequest.getTech());
        newTechnology.setDescription(createProjectRequest.getTechDescription());
        newTechnology.setLink(createProjectRequest.getTechLink());
        Technology technology = technologyRepository.save(newTechnology);

        newTeam.setArea(createProjectRequest.getTeamArea());
        newTeam.setRoom(createProjectRequest.getTeamRoom());
        newTeam.setName(createProjectRequest.getTeamName());
        newTeam.setProject(project);
        newTeam.setTechnology(technology);
        Team team = teamRepository.save(newTeam);

        return team.getId();
    }
}
