package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(value = "select distinct cast(offices.id as varchar), offices.city, offices.address " +
            "from offices left join users on users.office_id = offices.id " +
            "left join teams on teams.id = users.team_id " +
            "left join technologies on technologies.id = teams.technology_id " +
            "where technologies.name = 'Java'",
            nativeQuery = true)
    List<Object[]> findAllByTechnology(String technology);

    Optional<Office> findByAddressAndUsersNotNull(String address);
}
