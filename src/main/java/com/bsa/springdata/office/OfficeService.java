package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        // Use single query to get data. Sort by office name
        return officeRepository.findAllByTechnology(technology).stream()
                .map(elem -> new OfficeDto(
                        UUID.fromString((String) elem[0]),
                        (String) elem[1],
                        (String) elem[2]))
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        // Use single method to update address. In order to get the new office you can make extra query
        //  Hint: Every user is connected to one of the project. There cannot be any users without a project.
        Optional<Office> officeToUpdate = officeRepository.findByAddressAndUsersNotNull(oldAddress);
        if (officeToUpdate.isEmpty()) {
            return Optional.empty();
        }
        Office office = officeToUpdate.get();
        office.setAddress(newAddress);
        Office newOffice = officeRepository.save(office);
        return Optional.of(OfficeDto.fromEntity(newOffice));
    }
}
