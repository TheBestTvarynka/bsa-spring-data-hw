package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findAllByLastNameContainingIgnoreCase(String lastName, Pageable pagination);

    @Query("select u from User u where u.office.city = :city")
    List<User> findByCity(String city, Sort sorting);

    List<User> findAllByExperienceGreaterThanEqual(int experience, Sort sorting);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findByRoomAndCity(String city, String room, Sort sorting);

    List<User> findAllByExperienceLessThan(int experience);

}
