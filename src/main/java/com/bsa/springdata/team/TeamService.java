package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        // You can use several queries here. Try to keep it as simple as possible
        List<UUID> technologyIds = teamRepository.findAllTechnologiesForTeams(devsNumber, oldTechnologyName).stream()
                .map(obj -> (String) obj)
                .map(UUID::fromString)
                .collect(Collectors.toList());
        List<Technology> updatedTechnologies = technologyRepository.findAllById(technologyIds).stream()
                .peek(technology -> technology.setName(newTechnologyName))
                .collect(Collectors.toList());
        technologyRepository.saveAll(updatedTechnologies);
    }

    public void normalizeName(String teamName) {
        // Use a single query. You need to create a native query
         teamRepository.normalizeName(teamName);
    }
}
