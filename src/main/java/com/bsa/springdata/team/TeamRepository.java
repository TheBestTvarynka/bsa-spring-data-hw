package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Query(value = "select distinct cast(technologies.id as varchar) " +
            "from teams " +
            "left join technologies on technologies.id = teams.technology_id " +
            "left join (select teams.id as team_id, count(users.id) as dc from teams left join users on users.team_id = teams.id group by teams.id) as dcount on dcount.team_id = teams.id " +
            "where dcount.dc < :devCount and technologies.name = :technology",
    nativeQuery = true)
    List<Object> findAllTechnologiesForTeams(int devCount, String technology);

    @Modifying
    @Transactional
    @Query(value = "update teams as t set name = concat(t2.tname, '_', t2.pname, '_', t2.tchname) " +
            "from (select teams.id as id, teams.name as tname, projects.name as pname, technologies.name as tchname from teams left join projects on projects.id = teams.project_id left join technologies on technologies.id = teams.technology_id) as t2 " +
            "where t2.id = t.id " +
            "and t.name = :team",
    nativeQuery = true)
    void normalizeName(String team);
    
    @Query("select count(t) from Team t where t.technology.name = :name")
    int countByTechnologyName(String name);

    Optional<Team> findByName(String hipsters_facebook_javaScript);
}
